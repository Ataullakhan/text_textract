import re
import pandas as pd

from text_textract_app import format_utils
from text_textract import settings


format_lable = dict()
format_lable['Aetna_1'] = ("TX 799981106", 'aetna')
format_lable['Aetna_2'] = ("Claim Status Information", "Aetna")
format_lable['Aetna_3'] = ("Provider Payment Listing", "aetna", "Provider Denial Listing")
format_lable['Aetna_4'] = ("Dear Health Care Professional", "aetna")
format_lable['Aetna_5'] = ("Forwarding Service Requested", "Aetna")

format_lable['BCBS_1'] = ("WEEKLY PROVIDER PAYMENT REGISTER", 'BLUE CROSS')
# format_lable['BCBS_2'] = "GLOSSARY:"
format_lable['BCBS_3'] = ("Provider Remittance Advice", "BLUE CROSS")
format_lable['BCBS_4'] = ("Provider Voucher", "BLUE CROSS")

# format_lable['CIGNA_1'] = ""
format_lable['CIGNA_2'] = ("Forwarding Service Requested", "Cigna")
format_lable['CIGNA_3'] = ("Claims Payment Notification", "Cigna")
# format_lable['CIGNA_4'] = ("Explanation of Benefits", "Cigna")

format_lable['UHC_1'] = ("UNITED HEALTHCARE", "SERV DATE")
format_lable['UHC_2'] = ("Explanation of Payments", "UnitedHealthcare")
format_lable['UHC_3'] = ("Additional Notes", "UnitedHealthcare")
format_lable['UHC_4'] = ("CODE DESCRIPTIONS", "UNITEDHEALTHCARE")
format_lable['UHC_5'] = ("United Healthcare Community", "United Healthcare")

client_lable = ['aetna', 'cigna', 'unitedhealthcare', 'BLUE CROSS', 'united healthcare']


def find_payer_name(text):
    client_name = ''
    for i in client_lable:
        found = re.search(i, text.lower())
        if found:
            client_name = found.group()
    return client_name


def find_formate(text, page_format):
    text = re.sub(' +', ' ', text)
    # text = text.replace(' :', ':')
    # text = format_utils.text_correction(text)
    text = text.replace('\\', ' ')
    patient_name_l = []
    claim_id_l = []
    patient_acct_no_l = []
    format_dt = {'Payer_Name': get_payer_name(text)[0]}
    if not format_dt['Payer_Name']:
        format_dt['Payer_Name'] = find_payer_name(text) or None
    if page_format == 'Aetna_1':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna1_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['PT_RESP'] = aetna_pt_resp(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'Aetna_2':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_2_patient_name(text)
        if not patient_name_l:
            patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'Aetna_3':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'Aetna_4':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'Aetna_5':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'CIGNA_2':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'CIGNA_4':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = cigna_4_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'BCBS_1':
        format_dt['Check_No'] = bcbs1_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'BCBS_3':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = bcbs3_dos_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = bcbs3_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'BCBS_4':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = bcbs3_dos_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = bcbs3_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'UHC_3':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        if not format_dt['Patient_Name']:
            format_dt['Patient_Name'] = aetna1_patient_name(text)[0]
        format_dt['Processed_Date'] = bcbs3_dos_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    elif page_format == 'UHC_1':
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        if not format_dt['Patient_Name']:
            format_dt['Patient_Name'] = aetna1_patient_name(text)[0]
        format_dt['Processed_Date'] = bcbs3_dos_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
    else:
        format_dt['Check_No'] = aetna_check_no(text)[0]
        patient_name_l = aetna_get_patient_name(text)
        format_dt['Patient_Name'] = patient_name_l[0]
        if not format_dt['Patient_Name']:
            patient_name_l = aetna1_patient_name(text)
            format_dt['Patient_Name'] = patient_name_l[0]
        format_dt['Processed_Date'] = get_processed_date(text)[0]
        if not format_dt['Processed_Date']:
            format_dt['Processed_Date'] = aetna1_date(text)[0]
        format_dt['Payee_ID'] = atena_payee_id(text)[0]
        format_dt['Patient_Mem_ID'] = atena_pat_member_id(text)[0]
        patient_acct_no_l = aetna_patien_acc_no(text)
        format_dt['Patient_Acct_No'] = patient_acct_no_l[0]
        format_dt['Group_No'] = aetna_get_group_no(text)[0]
        format_dt['Provider_NPI'] = aetna_get_provider(text)[0]
        claim_id_l = aetna_claim_id(text)
        format_dt['Claim_Id'] = claim_id_l[0]
        format_dt['Check_Amount'] = aetna_check_amount(text)[0]
        format_dt['See_Remark'] = get_remark(text)[0]
        format_dt['GRP_RC_AMT'] = get_grp_rc_amt(text)[0]
        format_dt['Provider_Name'] = get_provider_name(text)[0]
        format_dt['Group_Name'] = get_group_name(text)[0]
        format_dt['Claim_Status'] = get_claim_status(text)[0]
        # format_dt['Payment_Status'] = get_payment_status(text)[0]
        format_dt['Not_Payable'] = get_not_payable_info(text)[0]
        format_dt['Co_Insurance'] = get_co_insurance_info(text)[0]
        format_dt['Deductible'] = get_deductible_info(text)[0]
        format_dt['CoPay'] = get_copay_info(text)[0]
        format_dt['Check_Date'] = check_date(text)[0]
        format_dt['Tax_ID'] = tax_id(text)[0]

    kvp_result = []
    for i in format_dt:
        kvp_result_dict = {'label': i, 'extraction': format_dt[i]}
        kvp_result.append(kvp_result_dict)
    return kvp_result


path = settings.MEDIA_ROOT
dfs = pd.read_csv(path + '/EOB_Bag_of_Words_Master_Updated_17thSept2021_T.csv')
provider_lable = [y for y in dfs['NPI'].values.tolist() if isinstance(y, str)]
check_no_lable = [y for y in dfs['Check No:'].values.tolist() if isinstance(y, str)]
patient_name_lable = [y for y in dfs['Patient Name'].values.tolist() if isinstance(y, str)]
Processed_Date_lable = [y for y in dfs['DATES OF SERVICE'].values.tolist() if isinstance(y, str)]
pat_member_id = [y for y in dfs['Patient ID No:'].values.tolist() if isinstance(y, str)]
patient_acc_no = [y for y in dfs['Patient Acct No:'].values.tolist() if isinstance(y, str)]

group_lable = [y for y in dfs['Group No:'].values.tolist() if isinstance(y, str)]
Claim_ID_lable = [y for y in dfs['Claim No:'].values.tolist() if isinstance(y, str)]
chk_amount_lable = [y for y in dfs['Amount'].values.tolist() if isinstance(y, str)]
dos_lable = [y for y in dfs['DATES OF SERVICE'].values.tolist() if isinstance(y, str)]

payer_name_lable = [y for y in dfs['Payer Name'].values.tolist() if isinstance(y, str)]
pt_resp = [y for y in dfs['PATIENT RESPONSIBILITY'].values.tolist() if isinstance(y, str)]
see_remark = [y for y in dfs['Remarks'].values.tolist() if isinstance(y, str)]
GRP_RC_AMT_lable = [y for y in dfs['GRP_RC_AMT'].values.tolist() if isinstance(y, str)]
Provider_name_lable = [y for y in dfs['Provider Name'].values.tolist() if isinstance(y, str)]

payee_id_lable = [y for y in dfs['Payer ID'].values.tolist() if isinstance(y, str)]
group_name_lable = [y for y in dfs['Group Name:'].values.tolist() if isinstance(y, str)]
claim_status_lable = [y for y in dfs['Claim Status'].values.tolist() if isinstance(y, str)]
# payment_status_lable = [y for y in dfs['Patient Status'].values.tolist() if isinstance(y, str)]
co_pay_lable = [y for y in dfs['COPAY AMOUNT'].values.tolist() if isinstance(y, str)]
deductible_lable = [y for y in dfs['Deductions\ Other Ineligible Amount'].values.tolist() if isinstance(y, str)]
co_insurance_lable = [y for y in dfs['CO INSURANCE'].values.tolist() if isinstance(y, str)]
not_payable_lable = [y for y in dfs['INELIGIBLE AMOUNT'].values.tolist() if isinstance(y, str)]
check_date_lable = [y for y in dfs['Check Date'].values.tolist() if isinstance(y, str)]
tax_id_lable = [y for y in dfs['Provider Tax ID'].values.tolist() if isinstance(y, str)]


def tax_id(text):
    info = []
    for i in tax_id_lable:
        pattern = '{}((.{{2,16}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def check_date(text):
    date_list = []
    for j in check_date_lable:
        if j != 'None':
            pattern = '{}(\s?(?:\d{{1,2}}(?:(?:-|/)|(?:th|st|nd|rd)?\s))?(?:(?:(?:jan(?:uary)?|feb(?:ruary)?|mar(' \
                      '?:ch)?|apr(?:il)?|may|jun(?:e)?|jul(?:y)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|nov(' \
                      '?:ember)?|dec(?:ember)?)(?:(?:-|/)|(?:,|\.)?\s)?)?(?:\d{{1,2}}(?:(?:-|/)|(' \
                      '?:,|th|st|nd|rd)?\s))?)(?:\d{{2,4}}))'.format(j)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}\s(\d+/\d+/\d+)'.format(j)

            pdf_text = re.finditer(pattern, text, re.IGNORECASE)
            for txt in pdf_text:
                match_txt1 = txt.group(1)
                if match_txt1 != '':
                    match_txt1 = match_txt1.replace(j, '').strip()
                    if hasNumbers(match_txt1) > 2:
                        date_list.append(match_txt1)
    if not date_list:
        date_list = [None]
    return date_list



def get_not_payable_info(text):
    info = []
    for i in not_payable_lable:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_co_insurance_info(text):
    info = []
    for i in co_insurance_lable:
        pattern = '{}\s((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_deductible_info(text):
    info = []
    for i in deductible_lable:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_copay_info(text):
    info = []
    for i in co_pay_lable:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_grp_rc_amt(text):
    info = []
    for i in GRP_RC_AMT_lable:
        pattern = '{}((.{{2,8}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1):
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_provider_name(text):
    info = []
    for i in Provider_name_lable:
        pattern = '{}((.\w+\s\w+)|(.\W\s\w+\s?\s\w+)|(.{{2,20}})|(\s+(\S+.{{1,10}})))'.format(i)
        if re.search(pattern, text, re.IGNORECASE) is None:
            pattern = '{}(.\W\s\w+\s?\s\w+)'.format(i)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}(.{{2,20}})'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if not hasNumbers(match_txt1):
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def get_group_name(text):
    info = []
    for i in group_name_lable:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


def get_claim_status(text):
    info = []
    for i in claim_status_lable:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


# def get_payment_status(text):
#     info = []
#     for i in payment_status_lable:
#         pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
#         pat_gen = re.finditer(pattern, text, re.IGNORECASE)
#         for txt in pat_gen:
#             match_txt1 = txt.group(1)
#             if match_txt1 != '':
#                 info.append(match_txt1)
#
#     if not info:
#         info = [None]
#     return info


def get_remark(text):
    info = []
    for i in see_remark:
        pattern = '{}((.{{2,8}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


def aetna_pt_resp(text):
    info = []
    for i in pt_resp:
        pattern = '{}((.\d{{1,6}}.\d{{1,5}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


def aetna_get_provider(text):
    info = []
    for i in provider_lable:
        pattern = '{}((.{{2,12}}\d+)|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


def aetna1_date(text):
    info = []
    pattern = 'date:(.\d+/\d+/\d+)'
    if re.search(pattern, text, re.IGNORECASE) is None:
        pattern = 'date:(.\s\d+\s\w+\s\d+)'
        if re.search(pattern, text, re.IGNORECASE) is None:
            pattern = 'date:(.+)'
    pat_gen = re.finditer(pattern, text, re.IGNORECASE)
    for txt in pat_gen:
        match_txt1 = txt.group(1)
        if match_txt1 != '':
            if hasNumbers(match_txt1) > 4 and len(match_txt1) < 35:
                match_txt1 = match_txt1.replace('date', '')
                info.append(match_txt1)
    if not info:
        info = [None]
    return info


def aetna1_patient_name(text):
    info = []
    pattern = 'name(\s[A-Z]+\S\W+[A-Z]+)'
    if re.search(pattern, text, re.IGNORECASE) is None:
        pattern = 'name:(.[A-Z]+\S\W+[A-Z]+)'
        if re.search(pattern, text, re.IGNORECASE) is None:
            pattern = 'patient(\\n(\S+\s+\w+))'
    pat_gen = re.finditer(pattern, text, re.IGNORECASE)
    for txt in pat_gen:
        match_txt1 = txt.group(1)
        if match_txt1 != '':
            match_txt1 = match_txt1.replace('name', '')
            match_txt1 = match_txt1.replace('patient', '')
            info.append(match_txt1)
    if not info:
        info = [None]
    return info


def aetna_2_patient_name(text):
    info = []
    pattern = 'patient name(.{2,20})'
    pat_gen = re.finditer(pattern, text, re.IGNORECASE)
    for txt in pat_gen:
        match_txt1 = txt.group(1)
        if match_txt1 != '':
            match_txt1 = match_txt1.replace('name', '')
            info.append(match_txt1)
    if not info:
        info = [None]
    return info


def get_processed_date(text):
    date_list = []
    for j in Processed_Date_lable:
        if j != 'None':
            pattern = '{}(\s?(?:\d{{1,2}}(?:(?:-|/)|(?:th|st|nd|rd)?\s))?(?:(?:(?:jan(?:uary)?|feb(?:ruary)?|mar(' \
                      '?:ch)?|apr(?:il)?|may|jun(?:e)?|jul(?:y)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|nov(' \
                      '?:ember)?|dec(?:ember)?)(?:(?:-|/)|(?:,|\.)?\s)?)?(?:\d{{1,2}}(?:(?:-|/)|(' \
                      '?:,|th|st|nd|rd)?\s))?)(?:\d{{2,4}}))'.format(j)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}(.\d+/\d+/\d+)'.format(j)

                if re.search(pattern, text, re.IGNORECASE) is None:
                    pattern = 'signed:(.+)'

            pdf_text = re.finditer(pattern, text, re.IGNORECASE)
            for txt in pdf_text:
                match_txt1 = txt.group(1)
                if match_txt1 != '':
                    match_txt1 = match_txt1.replace(j, '').strip()
                    if hasNumbers(match_txt1) > 4:
                        date_list.append(match_txt1)
    if not date_list:
        date_list = [None]
    return date_list


def aetna_check_no(text):
    info = []
    for i in check_no_lable:
        pattern = '{}((.{{2,10}}\d+)|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(':', '').strip()
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def aetna_check_amount(text):
    info = []
    for i in chk_amount_lable:
        pattern = '{}((.{{2,8}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(':', '').strip()
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def aetna_get_patient_name(text):
    patient_name = []
    for i in patient_name_lable:
        pattern = '{}((.\w+\s\w+)|(.\W\s\w+\s?\s\w+)|(.{{2,20}})|(\s+(\S+\s+\w+)))'.format(i)
        if re.search(pattern, text, re.IGNORECASE) is None:
            pattern = '{}(.\W\s\w+\s?\s\w+)'.format(i)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}(.{{2,20}})'.format(i)

        pat_name = re.findall(pattern, text, re.IGNORECASE)
        if pat_name:
            for k in pat_name:
                if 'claim status' not in k[0] and \
                        'city' not in k[0] and \
                        'health' not in k[0] and 'complete' not in k[0] and \
                        'id' not in k[0] and 'claim' not in k[0] and \
                        'dates of' not in k[0] and 'date' not in k[0] and \
                        'details' not in k[0] and 'rcvd' not in k[0] and \
                        'acct' not in k[0] and 'account number' not in k[0] and \
                        'information' not in k[0] and 'for' not in k[0] and\
                        'indicator' not in k[0] and 'amount' not in k[0] and\
                        'date' not in k[0] and 'subscriber' not in k[0] and 's name' not in k[0]:
                    match_txt1 = k[0].replace(i, '').strip()
                    match_txt1 = match_txt1.replace('provider', '').strip()
                    match_txt1 = match_txt1.replace('claim', '').strip()
                    match_txt1 = match_txt1.replace(':', '').strip()
                    if match_txt1 != '':
                        if hasNumbers(match_txt1):
                            pass
                        else:
                            patient_name.append(match_txt1)

    if not patient_name:
        patient_name = [None]
    patient_name = list(set(patient_name))
    return sorted(patient_name)


def atena_pat_member_id(text):
    text = text.replace('tax identification number', '')
    info = []
    for i in pat_member_id:
        pattern = '{}((.{{2,22}})|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(':', '').strip()
                match_txt1 = match_txt1.replace('subscriber', '')
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def atena_payee_id(text):
    info = []
    for i in payee_id_lable:
        pattern = '{}((.{{2,15}}\d+)|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def aetna_patien_acc_no(text):
    info = []
    for i in patient_acc_no:
        pattern = '{}((.{{2,15}}\d+\w)|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1) > 2:
                    match_txt1 = match_txt1.replace('patient id', '')
                    match_txt1 = match_txt1.replace('patient account', '')
                    match_txt1 = match_txt1.replace('patient acoounts', '')
                    match_txt1 = match_txt1.replace(':', '').strip()
                    info.append(match_txt1)
    if not info:
        info = [None]
    info = list(set(info))
    return info


def aetna_get_group_no(text):
    info = []
    for i in group_lable:
        pattern = '{}((.{{2,12}}\d+)|(\s+(\S+\s+)))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def aetna_claim_id(text):
    info = []
    Claim_ID_lable.append('clairo id:')
    Claim_ID_lable.append('claim number')
    for i in Claim_ID_lable:
        pattern = '{}((.{{2,30}})|(\s+(\S+\s+)(\d+)?))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if not 'status' in match_txt1 and \
                        not 'dates' in match_txt1 and \
                        'recoup' not in match_txt1 and \
                        'date' not in match_txt1 \
                        and 'subscriber' not in match_txt1 and \
                        'other insurance' not in match_txt1 and 'amount' not in match_txt1:
                    match_txt1 = match_txt1.replace('correc', '')
                    match_txt1 = match_txt1.replace('patient account', '')
                    match_txt1 = match_txt1.replace('account', '')
                    match_txt1 = match_txt1.replace('patient', '')
                    match_txt1 = match_txt1.replace('na hi', '')
                    match_txt1 = match_txt1.replace('ted:', '')
                    match_txt1 = match_txt1.replace('ted: na hi', '')
                    # if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)

    if not info:
        info = [None]
    info = list(set(info))
    return info


def cigna_4_claim_id(text):
    info = []
    for i in Claim_ID_lable:
        pattern = '{}((.{{2,16}}))'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def bcbs1_check_no(text):
    info = []
    for i in check_no_lable:
        pattern = '{}((.*\n){{2}}\d+)'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace('\n', ' ')
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)
    if not info:
        info = [None]
    return info


def bcbs3_claim_id(text):
    info = []
    for i in Claim_ID_lable:
        pattern = '{}(.[\d+]{{2,12}}\d+)'.format(i)
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)

    if not info:
        info = [None]
    return info


def bcbs3_dos_date(text):
    date_list = []
    for j in dos_lable:
        if j != 'None':
            pattern = '{}(\s?(?:\d{{1,2}}(?:(?:-|/)|(?:th|st|nd|rd)?\s))?(?:(?:(?:jan(?:uary)?|feb(?:ruary)?|mar(' \
                      '?:ch)?|apr(?:il)?|may|jun(?:e)?|jul(?:y)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|nov(' \
                      '?:ember)?|dec(?:ember)?)(?:(?:-|/)|(?:,|\.)?\s)?)?(?:\d{{1,2}}(?:(?:-|/)|(' \
                      '?:,|th|st|nd|rd)?\s))?)(?:\d{{2,4}}))'.format(j)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}\s(\d+/\d+/\d+)'.format(j)

                if re.search(pattern, text, re.IGNORECASE) is None:
                    pattern = 'signed:(.+)'

            pdf_text = re.finditer(pattern, text, re.IGNORECASE)
            for txt in pdf_text:
                match_txt1 = txt.group(1)
                if match_txt1 != '':
                    match_txt1 = match_txt1.replace(j, '').strip()
                    if hasNumbers(match_txt1) > 2:
                        date_list.append(match_txt1)
    if not date_list:
        date_list = [None]
    return date_list


def get_payer_name(text):
    payer_name = []
    for i in payer_name_lable:
        pattern = '{}((.\w+\s\w+)|(\s+(\S+\s+)))'.format(i)
        if re.search(pattern, text, re.IGNORECASE) is None:
            pattern = '{}(.\W\s\w+\s?\s\w+)'.format(i)

        pat_name = re.finditer(pattern, text, re.IGNORECASE)

        for txt in pat_name:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(i, '').strip()
                payer_name.append(match_txt1)

    if not payer_name:
        payer_name = [None]
    return payer_name


def hasNumbers(inputstring):
    count = 0
    for char in inputstring:
        if char.isdigit():
            count += 1
    return count


def waystar_total_claim_info(text):
    text = text.replace('\n', ' ')
    total_line = re.search('totals: #(.+)(clossary|provider)', text)
    # total_line = re.search('((?sm)^TOTALS:\n#.*(GLOSSARY|PROVIDER))', text)
    t_line = ''
    if not None:
        t_line = total_line.group()

    t_line = t_line.replace('. ', '.')
    all_numbers = re.findall(r"[-+]?\d*\.\d+|\d+", t_line)
    all_total_keys = ['# OF CLAIMS', 'BILLED AMT', ' ALLOWED AMT',
                      'DEDUCT AMT', 'COINS AMT', 'RC-AMT', 'PROV PAID',
                      'PROV ADJ', 'CHECK AMT']
    final_total_list = []
    for i, j in zip(all_total_keys, all_numbers):
        res_d = {'label': i.lower(), 'extraction': j}
        final_total_list.append(res_d)

    return final_total_list


def get_sequence_info(info_list, text):
    p_1 = {}
    for i in info_list:
        p_1[i] = find_str(text, i)
    p_2 = [k for k, v in sorted(p_1.items(), key=lambda item: item[1])]
    return p_2


def find_str(s, char):
    index = 0
    if char in s:
        c = char[0]
        for ch in s:
            if ch == c:
                if s[index:index+len(char)] == char:
                    return index
            index += 1
    return -1
