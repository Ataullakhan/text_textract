from django.shortcuts import render
from text_textract import settings
from django.core.files.storage import FileSystemStorage
from text_textract_app.utils import ocr_text_extract, patien_acc_no, check_no, claim_id, get_remark, check_date, doctr_text_extraction
from pdf2image import convert_from_path
from text_textract_app.format_utils import pdf_format
from text_textract_app.regex_kvp_utils import find_formate, waystar_total_claim_info
from text_textract_app.text_currection import page_text_correction, merge_forms_and_regex

import os
import re
import pandas as pd
import json
import filetype
path = settings.MEDIA_ROOT


def home(request):
    """
    :param request:
    :return:
    """
    text = ''
    text1 = ''
    doc_text = ''
    page_format = ''
    try:
        # form_list = []
        # if os.path.exists(path + '/output.csv'):
        #     os.remove(path + '/output.csv')
        if request.method == 'POST' and request.FILES['files']:
            myfile = request.FILES['files']
            fs = FileSystemStorage()
            if os.path.exists(path + '/' + myfile.name):
                os.remove(path + '/' + myfile.name)
                filename = fs.save(myfile.name, myfile)
                files = path + '/' + filename
                file_type = filetype.guess(files)
                if file_type.extension == 'pdf':
                    page_format = pdf_format(files)
                    pages3 = convert_from_path(files, 500)
                    for page in pages3:
                        page.save(path + '/converted.jpg', 'jpeg')
                    #     # third file extract
                        documentName = path +'/converted.jpg'

                        text += ocr_text_extract(documentName)
                        doc_text += doctr_text_extraction(documentName)

            else:
                filename = fs.save(myfile.name, myfile)
                files = path + '/' + filename

                file_type = filetype.guess(files)
                if file_type.extension == 'pdf':
                    page_format = pdf_format(files)
                    pages3 = convert_from_path(files, 500)
                    for page in pages3:
                        page.save(path + '/converted.jpg', 'jpeg')
                        #     # third file extract
                        documentName = path + '/converted.jpg'

                        text += ocr_text_extract(documentName)
                        doc_text += doctr_text_extraction(documentName)

            text1 = text.lower() + doc_text.lower()

            text2 = page_text_correction(text1)

            final_dict = get_form_value(text2)

            result_kvp_1 = find_formate(text2, page_format)

            new_result_kvp = {}
            ignore_list = ['Check_No', 'Claim_Id', 'Check_Date', 'Patient_Acct_No', 'See_Remark']
            for i in result_kvp_1:
                if i['label'] not in ignore_list:
                    new_result_kvp[i['label']] = i['extraction']

            final_dict1 = {**final_dict, **new_result_kvp}

            dir_name = path
            test = os.listdir(dir_name)
            for item in test:
                if item.endswith(".pdf"):
                    os.remove(os.path.join(dir_name, item))

        return render(request, 'home.html', {
            "filename": 'converted.jpg',
            "text": text1,
            "form_text": final_dict1,
            # "table_texract": table_texract
        })
    except Exception as e:
            print("Exception", e)

    return render(request, 'home.html')


def get_form_value(text):
    """

    :param lookup_dict:
    :param form_list:
    :return:
    """

    C_no = None
    C_date = None
    P_a_no = None
    claim_no = None

    C_A_C = re.findall('check number check date patient account # claim receipt date(.\s+(\S+\s+).+)', text)

    if len(C_A_C) == 0:
        C_A_C = re.findall('check number check date patient account #(\s+(\S+\s+).+)', text)

    if C_A_C:
        C_A_C1 = C_A_C[0][0].split()
        C_A_C1 = [x for x in C_A_C1 if x and len(x) > 3]
        C_no = C_A_C1[0]
        C_date = C_A_C1[1]
        P_a_no = C_A_C1[2]

    CL = re.findall('claim(.+)', text)
    if CL:
        CL = [x for x in CL if x]
        claim = CL[0].split()
        claim = [x for x in claim if x and len(x) > 3]
        claim_no = claim[0]

    final_dict = {}

    AccNo = patien_acc_no(text)

    if AccNo[0] != None:
        final_dict['AccNo'] = AccNo[0]
    else:
        final_dict['AccNo'] = P_a_no
    ClaimNo = claim_id(text)
    if ClaimNo[0] != None:
        final_dict['ClaimNo'] = ClaimNo[0]
    else:
        final_dict['ClaimNo'] = claim_no
    
    CheckDate = check_date(text)
    if CheckDate[0] != None:
        final_dict['CheckDate'] = CheckDate[0]
    else:
        final_dict['CheckDate'] = C_date

    CheckNo = check_no(text)
    if CheckNo[0] != None:
        final_dict['CheckNo'] = CheckNo[0]
    else:
        final_dict['CheckNo'] = C_no

    Remark_or_DenialCodes = get_remark(text)
    final_dict['Remark_or_DenialCodes'] = Remark_or_DenialCodes[0]
    return final_dict


def get_information(text, lable):
    try:
        pattern_creation='|'.join(lable)
        info_extraction =r"("+pattern_creation+")(.{2,20})"
        extraction=re.findall(info_extraction, text)
        info1_extraction=extraction[-1][-1]
        return info1_extraction
    except:
        return ''
