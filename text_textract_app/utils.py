from PIL import Image
from PIL import ImageEnhance
import re
import math
from collections import Counter
from text_textract import settings
from pytesseract import pytesseract
import pandas as pd
import numpy as np
import cv2
import matplotlib.pyplot as plt

from doctr.io import DocumentFile
from doctr.models import ocr_predictor
# import os

model = ocr_predictor(det_arch='db_resnet50',  reco_arch='crnn_vgg16_bn', pretrained=True)

path = settings.MEDIA_ROOT


dfs = pd.read_csv(path + '/EOB_Bag_of_Words_Master Updated_Sep_6_2021_T.csv')
Claim_ID_lable = [y for y in dfs['Claim No:'].values.tolist() if isinstance(y, str)]
check_no_lable = [y for y in dfs['Check No:'].values.tolist() if isinstance(y, str)]
patient_acc_no = [y for y in dfs['Patient Acct No:'].values.tolist() if isinstance(y, str)]
check_date_lable = [y for y in dfs['Processed On:'].values.tolist() if isinstance(y, str)]
remark_lable = [y for y in dfs['Remarks'].values.tolist() if isinstance(y, str)]


def ocr_text_extract(documentName):
    image = enhance_image(documentName)
    text = pytesseract.image_to_string(image, lang='eng',
                                       config='--psm 4 -c tessedit_char_blacklist=~!@%^£|;}][©€')

    text = text.lower()
    text = re.sub('  +', ' ', text)
    return text


def doctr_text_extraction(documentName):
    degree = pytesseract.image_to_osd(Image.open(documentName))
    if "Rotate: 90" in degree:
        src = cv2.imread(documentName)
        img = cv2.rotate(src, cv2.ROTATE_90_CLOCKWISE)
        plt.imsave(path + "/Py_Rotated_image.png", img)
        rotate_image = path + "/Py_Rotated_image.png"
    elif "Rotate: 270" in degree:
        src = cv2.imread(documentName)
        img = cv2.rotate(src, cv2.ROTATE_90_COUNTERCLOCKWISE)
        plt.imsave(path + "/Py_Rotated_image.png", img)
        rotate_image = path + "/Py_Rotated_image.png"
    else:
        rotate_image = documentName

    img = DocumentFile.from_images(rotate_image)
    result = model(img)
    text = result.render()

    text = str(text).lower()
    text = re.sub('  +', ' ', text)

    return text


def check_no(text):
    info = []
    for i in check_no_lable:
        pattern = f"{i.lower()}(.{{2,10}}\d+|\s+(\S+\s+))"
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(i, '').strip()
                match_txt1 = re.split('\s+|-|,|\\n', match_txt1)
                match_txt1 = [x for x in match_txt1 if x]
                match_txt1 = match_txt1[0].replace(':', '').strip()
                if hasNumbers(match_txt1) > 2:
                    info.append(match_txt1)

    return info or [None]


def patien_acc_no(text):
    info = []
    for i in patient_acc_no:
        pattern = f"{i.lower()}(.{{2,15}}\d+\w|\s+(\S+\s+))"
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1:
                match_txt1 = match_txt1.replace(i.lower(), "").strip()
                match_txt1 = match_txt1.replace("number", "").strip()
                if hasNumbers(match_txt1) > 2:
                    match_txt1 = match_txt1.replace("patient id", "").strip()
                    match_txt1 = match_txt1.replace("patient account", "").strip()
                    match_txt1 = match_txt1.replace("patient acoounts", "").strip()
                    match_txt1 = match_txt1.replace(":", "").strip()
                    match_txt1 = re.split(r"\s+|-|,|\\n", match_txt1)
                    match_txt1 = [x for x in match_txt1 if x]
                    info.append(match_txt1[0])

    return info or [None]



def claim_id(text):
    info = []
    Claim_ID_lable.append('clairo id:')
    Claim_ID_lable.append('claim number')
    for i in Claim_ID_lable:
        pattern = '{}((.{{2,30}})|(\s+(\S+\s+)(\d+)?))'.format(i.lower())
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(i, '').strip()
                match_txt1 = re.split('\s+|-|,|\\n|:', match_txt1)
                match_txt1 = [x for x in match_txt1 if x]
                # if hasNumbers(match_txt1) > 2:
                info.append(match_txt1[0])

    if not info:
        info = [None]
    return info


def get_remark(text):
    info = []
    for i in remark_lable:
        pattern = '{}((.{{2,8}})|(\s+(\S+\s+)))'.format(i.lower())
        pat_gen = re.finditer(pattern, text, re.IGNORECASE)
        for txt in pat_gen:
            match_txt1 = txt.group(1)
            if match_txt1 != '':
                match_txt1 = match_txt1.replace(i, '').strip()
                match_txt1 = re.split('\s+|-|,|\\n', match_txt1)
                match_txt1 = [x for x in match_txt1 if x]
                info.append(match_txt1)

    if not info:
        info = [None]
    return info


def check_date(text):
    date_list = []
    for j in check_date_lable:
        if j != 'None':
            pattern = '{}(\s?(?:\d{{1,2}}(?:(?:-|/)|(?:th|st|nd|rd)?\s))?(?:(?:(?:jan(?:uary)?|feb(?:ruary)?|mar(' \
                      '?:ch)?|apr(?:il)?|may|jun(?:e)?|jul(?:y)?|aug(?:ust)?|sep(?:tember)?|oct(?:ober)?|nov(' \
                      '?:ember)?|dec(?:ember)?)(?:(?:-|/)|(?:,|\.)?\s)?)?(?:\d{{1,2}}(?:(?:-|/)|(' \
                      '?:,|th|st|nd|rd)?\s))?)(?:\d{{2,4}}))'.format(j)
            if re.search(pattern, text, re.IGNORECASE) is None:
                pattern = '{}\s(\d+/\d+/\d+)'.format(j.lower())

            pdf_text = re.finditer(pattern, text, re.IGNORECASE)
            for txt in pdf_text:
                match_txt1 = txt.group(1)
                if match_txt1 != '' or match_txt1 is not None:
                    match_txt1 = re.split('\s+|-|,|\\n', match_txt1)
                    match_txt1 = [x for x in match_txt1 if x]
                    match_txt1 = match_txt1[0].replace(j, '').strip()
                    if hasNumbers(match_txt1) > 2:
                        if len(match_txt1) > 4:
                            date_list.append(match_txt1)

    return  date_list or [None]


def hasNumbers(inputstring):
    count = 0
    for char in inputstring:
        if char.isdigit():
            count += 1
    return count


def enhance_image(page):
    img = cv2.imread(page, cv2.IMREAD_GRAYSCALE)
    morphed = cv2.morphologyEx(img, cv2.MORPH_CLOSE, np.ones((1, 40), np.uint8))
    dst = cv2.add(img, 255 - morphed)
    dst = cv2.resize(dst, None, fx=1.35, fy=1.35, interpolation=cv2.INTER_CUBIC)
    thresh = cv2.threshold(dst, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    return thresh


def get_date_info(text):
    date_reg_exp2 = re.compile(r'\d{2}([-/.])(\d{2}|[a-zA-Z]{3})\1(\d{4}|\d{2})|\w{2,12}\s\d{2}[,.]\s\d{4}')
    matches_list = [x.group() for x in date_reg_exp2.finditer(text)]
    return matches_list


def get_numeric_info(text):
    info = []
    if text.find("cla") != -1:
        pattern = 'cla(.+)'
    elif text.find("che") != -1:
        pattern = 'che(.+)'
    elif text.find("npi") != -1:
        pattern = 'np(.+)'
    elif text.find("eft") != -1:
        pattern = 'eft(.+)'
    elif text.find("icn") != -1:
        pattern = 'icn(.+)'
    else:
        pattern = '([A-Z]{1,6}\d(.+)|\d(.+))'

    pat_gen = re.finditer(pattern, text)

    for txt in pat_gen:
        match_txt1 = txt.group()
        info.append(match_txt1)
    return info


