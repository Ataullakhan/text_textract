import pytesseract
from pdf2image import convert_from_bytes
import re
from PIL import Image
import numpy as np
import cv2
import editdistance

from text_textract_app.regex_kvp_utils import format_lable
from text_textract import settings

path = settings.MEDIA_ROOT
import matplotlib.pyplot as plt


def pdf_format(file_name):
    pages3_1 = convert_from_bytes(open(file_name,'rb').read())
    text = ''
    pages3 = pages3_1[:3]
    for page in pages3:
        page.save(path + '/_page.jpg', 'jpeg')
        page_name = path + '/_page.jpg'
        try:
            degree = pytesseract.image_to_osd(Image.open(page_name))
            if "Rotate: 90" in degree:
                src = cv2.imread(page_name)
                img = cv2.rotate(src, cv2.cv2.ROTATE_90_CLOCKWISE)

                plt.imsave(path + "/Py_Rotated_image.png", img)
                rotate_image = path + "/Py_Rotated_image.png"
            elif "Rotate: 270" in degree:
                src = cv2.imread(page_name)
                img = cv2.rotate(src, cv2.cv2.ROTATE_90_COUNTERCLOCKWISE)

                plt.imsave(path + "/Py_Rotated_image.png", img)
                rotate_image = path + "/Py_Rotated_image.png"
            else:
                rotate_image = page_name
        except:
            rotate_image = page_name

        config = '--psm 6'
        img = np.array(Image.open(rotate_image))
        text += pytesseract.image_to_string(img, lang='eng', config=config)
    page_format = find_page_format(text)
    return page_format


def find_page_format(text):
    page_format = ''
    for i in format_lable:
        found1 = re.search(format_lable[i][0], text, re.IGNORECASE)
        found2 = re.search(format_lable[i][1], text, re.IGNORECASE)

        if len(format_lable[i]) == 3:
            found3 = re.search(format_lable[i][2], text, re.IGNORECASE)
            if found1 and found2:
                page_format = i
            elif found2 and found3:
                page_format = i
        if found1 and found2:
            page_format = i

    return page_format


def dark_img_procesing(image):
    img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    img = cv2.multiply(img, 3)
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.erode(img, kernel, iterations=1)

    return img


def table_dark_img_procesing(image):
    img = cv2.imread(image, cv2.IMREAD_GRAYSCALE)
    img = cv2.multiply(img, 0.5)
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.erode(img, kernel, iterations=1)

    return img


def text_correction(text):
    str_1 = re.findall(r"[\w']+", text)
    for i in str_1:
        if editdistance.eval('patient', i) <= 3:
            text = text.replace(i, 'patient')
        if editdistance.eval('provider', i) <= 3:
            text = text.replace(i, 'provider')
        if editdistance.eval('claim', i) <= 1:
            text = text.replace(i, 'claim')
        if editdistance.eval('account', i) <= 1:
            text = text.replace(i, 'account')
        if editdistance.eval('amount', i) <= 1:
            text = text.replace(i, 'amount')
        if editdistance.eval('name', i) <= 1:
            text = text.replace(i, 'name')
        if editdistance.eval('check', i) <= 1:
            text = text.replace(i, 'check')
        if editdistance.eval('payee', i) <= 1:
            text = text.replace(i, 'payee')
        if editdistance.eval('npi', i) <= 1:
            text = text.replace(i, 'npi')
        if editdistance.eval('provider npi', i) <= 3:
            text = text.replace(i, 'provider npi')
    return text