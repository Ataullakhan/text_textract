from django.apps import AppConfig


class TextTextractAppConfig(AppConfig):
    name = 'text_textract_app'
